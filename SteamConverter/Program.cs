namespace SteamConverter
{
    static partial class Program
    {
        [STAThread]

        static void Main()
        {
            using (OpenFileDialog ofd = new OpenFileDialog { Filter = "SADX executable|sonic.exe;Sonic Adventure DX.exe" })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    SADXPatcher.PatchGame(ofd.FileName, out string result);
                    MessageBox.Show(new Form() { WindowState = FormWindowState.Maximized, TopMost = true }, result, "SADX Steam Converter", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }
    }
}