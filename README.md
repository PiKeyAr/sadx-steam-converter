# SADX Steam Converter

This program can patch various versions of Sonic Adventure DX to make them compatible with the SADX Mod Loader. The conversion is done via patching the executable and other assets using a [C# implementation](https://github.com/neon-nyan/Hi3Helper.SharpHDiffPatch) of [HDiffPatch](https://github.com/sisong/HDiffPatch).

Supported versions:
- Steam / Dreamcast Collection Remastered
- 2004 (EU)
- 2004 (Best Buy)
- 2004 (Korean) - the game will be converted to a regular US version, and Korean language assets will be removed
- 2009 (Sonic PC Collection)

Unsupported versions:
- Dreamcast Collection 1.0
- 2004 (Japanese)
- 2004 (Sold Out Software) - probably
- Any versions that supply a modified or cracked sonic.exe

The patcher doesn't convert FMVs or sound effects. If it is used on the Steam version, the game will be unable to play movies, voices and sound effects without the Mod Loader installed.